# .bash_aliases

# Some example alias instructions

# If these are enabled they will be used instead of any instructions they may
# mask. For example, alias rm='rm -i' will mask the rm application. To override
# the alias instruction use a \ before, ie \rm will call the real rm not the
# alias.

#-------------------------------------------------------------------------------
# General Purpose
#-------------------------------------------------------------------------------
# ls
# 'LC_COLLATE=C' tells 'ls' not to ignore leading '.'s when sorting
alias ls='LC_COLLATE=C ls --color=auto --group-directories-first'
alias l.='ls -d .*'
alias ll='ls -l'
alias ll.='ll -d .*'
alias la='ls -lA'

# grep
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# shell commands
alias cls='printf "\033c"'
alias tree='LC_COLLATE=C tree --dirsfirst -C -I .git'

#-------------------------------------------------------------------------------
# Development
#-------------------------------------------------------------------------------
# git
if [ -x /usr/bin/git ]; then
  alias l='git log --oneline -n10'
  alias s='git status'
  alias si='git status --ignored'
  alias ss='git status -s'
  alias ssi='git status -s --ignored'
fi
