# ~/.bash_profile

. ~/.bashrc
utilities::prepend_path ~/bin
utilities::prepend_path ~/.local/bin
utilities::append_path  ${DOTFILES_DIR}/bin
utilities::append_path  ${FIREFLY_DIR}/bin
