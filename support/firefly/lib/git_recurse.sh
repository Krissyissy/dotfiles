#!/bin/bash
set -e
#-------------------------------------------------------------------------------
# Filename: git_recurse.sh
#
# Description:
# Contains a set of functions to execute commands across multiple git working
# directories.
#
# Intended Usage:
# git-recurse [OPTIONS] [COMMAND] [ARG...]
#-------------------------------------------------------------------------------
LIBRARY_PATH=$(cd "${BASH_SOURCE[0]%/*}" && pwd)
source "${LIBRARY_PATH}/utilities.sh"

# Starting point to look for git working directories
declare BASE_DIR=.

# Regular expression passed to egrep to exclude git working directories
# The default value will match nothing
declare EXCLUDE_PATTERN="a^"

# Internal variables
declare -a _ALT_CMD
declare    _GIT_CMD="git"
declare -a _GIT_ARGS
declare    _EXE="git_recurse::execute_in_repos"
declare    _EXE_CMD="git_recurse::execute_cmd"

#-------------------------------------------------------------------------------
# Append command line arguments passed to the git command.
#
# Arguments:
# $@ - command line arguments to append to _GIT_ARGS
#-------------------------------------------------------------------------------
function git_recurse::append_git_args()
{
   utilities::append_array _GIT_ARGS "$@"
}

#-------------------------------------------------------------------------------
# Append command line arguments passed to an alternate command.
#
# Arguments:
# $@ - command line arguments to append to _ALT_CMD
#-------------------------------------------------------------------------------
function git_recurse::append_alt_cmd()
{
   utilities::append_array _ALT_CMD "$@"
}

#-------------------------------------------------------------------------------
# Find git working directories.
#
# Arguments:
# $1 - base directory
#
# Usage:
# local repos=( $(git_recurse::find_repos "${base_dir}") )
#-------------------------------------------------------------------------------
function git_recurse::find_repos()
{
   local -r base_dir="$1"
   echo $(find "${base_dir}" -name .git -prune \
            | sed 's|/\.git$||'                \
            | egrep -v "${EXCLUDE_PATTERN}"    \
            | sort)
}

#-------------------------------------------------------------------------------
# Returns the console output from running the 'git status' command with the
# options that were provided on the command line.
#
# An empty string will be returned if the repo is clean.
#
# Arguments:
# $1 - git repository
#
# Usage:
# if [[ -n "$(git_recurse::get_repo_status ${repo})" ]]; then
#    echo "dirty"
# else
#    echo "clean"
# fi
#-------------------------------------------------------------------------------
function git_recurse::get_repo_status()
{
   local -r repo="$1"
   pushd "${repo}" &> /dev/null
   local -r status="$("${_GIT_CMD}" "${_GIT_ARGS[@]}")"
   popd &> /dev/null
   echo "${status}"
}

#-------------------------------------------------------------------------------
# Print command line usage information.
#
# Arguments:
# None
#-------------------------------------------------------------------------------
function git_recurse::usage()
{
   local -r command="${0##*/}"

   cat <<EOF

Usage: $command [OPTIONS] [GIT SUBCOMMAND] [ARG...]

Run a git command across multiple git working directories.

Usage: $command [OPTIONS] --alt_cmd [COMMAND] [ARG...]

Run an arbitrary command across multiple git working directories

Options:
 -h, --help                   Print usage
     --alt_cmd string         Specify an alternate command
     --base                   Specifiy the base directory
     --dry_run                Print commands without executing them
     --exclude string         Pattern to exclude when searching for git working directories

All other options will be forwarded to the command being executed.
EOF
}

#-------------------------------------------------------------------------------
# Parse command line arguments.
#
# Arguments:
# $@ - command line arguments
#-------------------------------------------------------------------------------
function git_recurse::parse_cmd_line()
{
   local status

   while [[ $# -gt 0 ]]; do

      case "$1" in

         --alt_cmd)
            shift
            git_recurse::append_alt_cmd "$@"
            break
            ;;

         --base)
            BASE_DIR="$2"
            shift 2
            ;;

         --base=*)
            BASE_DIR="${1#*=}"
            shift
            ;;

         --dry_run)
            _EXE_CMD="git_recurse::dry_run"
            shift
            ;;

         --exclude)
            EXCLUDE_PATTERN="$2"
            shift 2
            ;;

         --exclude=*)
            EXCLUDE_PATTERN="${1#*=}"
            shift
            ;;

         -h|--help)
            git_recurse::usage
            exit
            ;;

         status)
            status="$1"
            git_recurse::append_git_args "$1"
            shift
            ;;

         -s|--short)
            status+=" -s"
            git_recurse::append_git_args "$1"
            shift
            ;;

         *)
            git_recurse::append_git_args "$1"
            shift
            ;;

      esac

   done

   if [[ "${_EXE_CMD}" != "git_recurse::dry_run" ]]; then
      # The command line contained both 'status' and '-s'
      if [[ "${status}" == "status -s" ]]; then
         _EXE="git_recurse::status_repos"
      fi
   fi
}

#-------------------------------------------------------------------------------
# Same as the execute_cmd() function but print the commands rather than execute
# them.
#
# Arguments:
# None
#-------------------------------------------------------------------------------
function git_recurse::dry_run()
{
   if [[ -n "${_ALT_CMD}" ]]; then
      echo "${_ALT_CMD[*]}"
      echo
      echo "Command:"
      for arg in "${_ALT_CMD[@]}"; do
         echo "${arg}"
      done
   else
      echo "${_GIT_CMD} ${_GIT_ARGS[*]}"
      echo
      echo "Command:"
      echo "${_GIT_CMD}"
      for arg in "${_GIT_ARGS[@]}"; do
         echo "${arg}"
      done
   fi
}

#-------------------------------------------------------------------------------
# Execute git command.
#
# Arguments:
# None
#-------------------------------------------------------------------------------
function git_recurse::execute_cmd()
{
   if [[ -n "${_ALT_CMD}" ]]; then
      "${_ALT_CMD[@]}"
   else
      "${_GIT_CMD}" "${_GIT_ARGS[@]}"
   fi
}

#-------------------------------------------------------------------------------
# Execute git command in the specified git repository.
#
# Arguments:
# $1 - git repository
#-------------------------------------------------------------------------------
function git_recurse::execute_in_repo()
{
   local repo=$1

   echo
   echo -e "${GREEN}#--------------------------------------------------------------------------------"
   echo -e "# ${YELLOW} ${repo} ${GREEN}"
   echo -e "#--------------------------------------------------------------------------------${NO_COLOR}"
   pushd "${repo}" &> /dev/null
   ${_EXE_CMD}
   popd &> /dev/null
}

#-------------------------------------------------------------------------------
# Execute git command in each git repository specified.
#
# Arguments:
# $@ - array of git repositories
#-------------------------------------------------------------------------------
function git_recurse::execute_in_repos()
{
   local repos=("${@}")

   # Loop through each repo and execute command
   for repo in "${repos[@]}"; do
      git_recurse::execute_in_repo "${repo}"
   done
}

#-------------------------------------------------------------------------------
# Execute git command in each git repository specified. The git command will
# only be executed if the repository is dirty. This function assumes that the
# git command to be executed is 'git status'.
#
# Arguments:
# $@ - array of git repositories
#-------------------------------------------------------------------------------
function git_recurse::status_repos()
{
   local repos=("${@}")
   local dirtyRepoCount=0

   # Loop through each repo and check its status
   for repo in "${repos[@]}"; do

      if [[ -n "$(git_recurse::get_repo_status ${repo})" ]]; then
         let dirtyRepoCount=dirtyRepoCount+1
         git_recurse::execute_in_repo "${repo}"
      fi

   done

   # All repositories were clean
   if (( ${dirtyRepoCount} == 0 )); then
      echo -e "${GREEN}All ${#repos[@]} repositories are clean${NO_COLOR}"
   fi
}

#-------------------------------------------------------------------------------
# Execute git command in each git repository under the base directory.
#
# Arguments:
# None
#-------------------------------------------------------------------------------
function git_recurse::execute()
{
   # The base directory does not exist
   if [[ ! -d "${BASE_DIR}" ]]; then
      utilities::error "'${BASE_DIR}' not found!"
      exit 1
   fi

   echo "Base directory: ${BASE_DIR}"

   # Find git working directories
   local -a repos=( $(git_recurse::find_repos "${BASE_DIR}") )

   # No git repositories were found
   if (( ${#repos[@]} == 0 )); then
      echo -e "${RED}No repositories found${NO_COLOR}"
      return
   else
      echo "${#repos[@]} repositories found"
   fi

   ${_EXE} "${repos[@]}"
}

#-------------------------------------------------------------------------------
# Parse the command line arguments and execute recursively.
#
# Arguments:
# $@ - command line arguments
#-------------------------------------------------------------------------------
function git_recurse::run()
{
   if (( $# > 0 )); then
      git_recurse::parse_cmd_line "$@"
   fi

   git_recurse::execute
}
