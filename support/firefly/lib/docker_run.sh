#!/bin/bash
set -e
#-------------------------------------------------------------------------------
# Filename: docker_run.sh
#
# Description:
# Contains a set of functions to manage the command line arguments passed to the
# 'docker run' command.
#
# Intended Usage:
# docker-run [OPTIONS] -- [COMMAND] [ARG...]
#
# Details:
# The 'docker run' command is decomposed into three parts: _OPTIONS, IMAGE, and
# _CMD. Each part can be controlled through a combination of configuration
# variables and command line options. When an option can be controlled by both a
# configuration variable and a command line option, the command line option will
# override the configuration variable.
#
# IMAGE
# --image
# The docker image can be specified using either the IMAGE configuration
# variable or the command line option.
#
# CONTAINER_NAME
# --name
# The container name can be specified using either the CONTAINER_NAME
# configuration variable or the command line option. The configuration variable
# may be set by other functions to automate container names.
#
# DEFAULT_COMMAND
# The default command can be specified using this configuration variable.
#
# If -it is present, the CONTAINER_NAME and DEFAULT_COMMAND will be applied if
# they are defined and they are not otherwise specified.
#
# Additional Options:
# There are a number of options that come up frequently that are required to
# setup a docker container for a particular use. This library contains
# additional functions, configuration variables, and command line options to
# simplify setting up a docker container. These additional options can be
# controlled by calling the appropriate functions directly, or through the
# command line options.
#
# docker_run::add_gdb_options()
# --include-gdb
# Apply conatiner options to use GDB outside the container.
#
# docker_run::add_icepic_options()
# --include-icepic
# Apply container options to use an ICEPIC device in the container.
#
# WORKSPACE_HOST_PATH
# WORKSPACE_DIR
# WORKSPACE_CONTATINER_PATH
# docker_run::add_workspace_options()
# --include-workspace
# Map a volume to a host directory and make it the workspace. A default
# container name will be created if a container name has not been specified.
#
# -n|--dry_run
# Print the 'docker run' command without executing it.
#-------------------------------------------------------------------------------
LIBRARY_PATH=$(cd "${BASH_SOURCE[0]%/*}" && pwd)
source "${LIBRARY_PATH}/utilities.sh"

# The name of the image to be used for the container.
# This value can be set directly or using the '--image' option on the command
# line.
declare IMAGE

# The name to give to the container using the '--name' option.
# Set value to override assigninment elsewhere.
# The container name can also be set using the '--name' option on the command
# line.
declare CONTAINER_NAME

# Default command if '-it' is present and a [COMMAND] [ARG..] is not provided on
# the command line.
# Set value to an empty array to disable.
declare -a DEFAULT_COMMAND=( bash )

# Workspace Parameters
# A workspace is a convenient way to mount a host directory inside the container
# and making that the working directory. The default is to mount the current
# directory on the host to /workspace/<dir> in the container where <dir> is
# the dirname of the current directory on the host.
declare WORKSPACE_HOST_PATH=$(pwd)
declare WORKSPACE_DIR="${WORKSPACE_HOST_PATH##*/}"
declare WORKSPACE_CONTATINER_PATH=/workspace/"${WORKSPACE_DIR}"

# Internal variables
declare -a _OPTIONS
declare -a _CMD
declare    _EXE="docker_run::execute"

#-------------------------------------------------------------------------------
# Append command line options to the 'docker run' command.
#
# Arguments:
# $@ - command line options to append to _OPTIONS
#-------------------------------------------------------------------------------
function docker_run::append_option()
{
   utilities::append_array _OPTIONS "$@"
}

#-------------------------------------------------------------------------------
# Prepend command line options to the 'docker run' command.
#
# Arguments:
# $@ - command line options to prepend to _OPTIONS
#-------------------------------------------------------------------------------
function docker_run::prepend_option()
{
   utilities::prepend_array _OPTIONS "$@"
}

#-------------------------------------------------------------------------------
# Append command/args to the command passed to 'docker run'.
#
# Arguments:
# $@ - command/args to append to _CMD
#-------------------------------------------------------------------------------
function docker_run::append_cmd()
{
   utilities::append_array _CMD "$@"
}

#-------------------------------------------------------------------------------
# Determine if an option is present in _OPTIONS.
#
# Arguments:
# $1 - string to search for
#-------------------------------------------------------------------------------
function docker_run::has_option()
{
   [[ "${_OPTIONS[*]}" =~ (^| )"$1"( |=|$) ]]
}

#-------------------------------------------------------------------------------
# Use "bash --login" as the default command.
#
# Arguments:
# None
#-------------------------------------------------------------------------------
function docker_run::use_bash_login()
{
   DEFAULT_COMMAND=( bash --login )
}

#-------------------------------------------------------------------------------
# Add options to forward ${SSH_AUTH_SOCK} into the container.
#
# Arguments:
# None
#-------------------------------------------------------------------------------
function docker_run::add_ssh_auth_sock_options()
{
   if [[ -z ${SSH_AUTH_SOCK} ]]; then
      utilities::print_error "SSH_AUTH_SOCK is not defined."
      utilities::print_error "Use \"eval \$(ssh-agent) && ssh-add\" to define."
      utilities::error
   fi

   docker_run::append_option --volume ${SSH_AUTH_SOCK}:/ssh-agent
   docker_run::append_option --env SSH_AUTH_SOCK=/ssh-agent
}

#-------------------------------------------------------------------------------
# Add container options required to use gdb from outside the container
# (e.g. from vscode).
#
# Arguments:
# None
#-------------------------------------------------------------------------------
function docker_run::add_gdb_options()
{
   docker_run::append_option --cap-add=SYS_PTRACE
}

#-------------------------------------------------------------------------------
# Provide access to the first ICEPIC device (device 0).
#
# Arguments:
# None
#-------------------------------------------------------------------------------
function docker_run::add_icepic_options()
{
   if [ -c /dev/icepic0 ]; then
      echo "ICEPIC detected."
      docker_run::append_option --device /dev/icepic0:/dev/icepic0
      docker_run::append_option --device /dev/pmem:/dev/pmem
      docker_run::append_option --shm-size=2500m
   else
      echo "ICEPIC not detected."
   fi
}

#-------------------------------------------------------------------------------
# Map a volume to a host directory and make that the workspace inside the
# container. A default container name will be created if a container name has
# not been specified.
#
# Arguments:
# None
#-------------------------------------------------------------------------------
function docker_run::add_workspace_options()
{
   docker_run::append_option -v "${WORKSPACE_HOST_PATH}":"${WORKSPACE_CONTATINER_PATH}"
   docker_run::append_option -w "${WORKSPACE_CONTATINER_PATH}"
   CONTAINER_NAME=${CONTAINER_NAME:="${USER}-${WORKSPACE_DIR}"}
}

#-------------------------------------------------------------------------------
# Print command line usage information.
#
# Arguments:
# None
#-------------------------------------------------------------------------------
function docker_run::usage()
{
   local -r command="${0##*/}"

   cat <<EOF

Usage: $command [OPTIONS] -- [COMMAND] [ARG...]

Create and run a new container from an image.

Options:
EOF

   docker_run::print_usage_options
}

#-------------------------------------------------------------------------------
# Print command line usage options.
#
# Arguments:
# None
#-------------------------------------------------------------------------------
function docker_run::print_usage_options()
{
   cat <<EOF
 -h, --help                   Print usage
     --image string           Use docker image
     --include-gdb            Apply conatiner options to use GDB outside the container
     --include-icepic         Apply container options to use an ICEPIC device in the container
     --include-workspace      Map a volume to a host directory and make it the workspace
     --login                  Use \"bash --login\" as the default command
 -n, --dry-run                Print the docker run command without executing it
 -s, --ssh-auth-sock          Forward \$SSH_AUTH_SOCK into the container

All other options will be forwarded to the actual docker run command.

If -it is present, the default --name and [COMMAND] [ARG...] will be applied if the defaults
are defined and they are not otherwise specified.====
EOF
}

#-------------------------------------------------------------------------------
# Parse command line arguments.
#
# Arguments:
# $@ - command line arguments
#-------------------------------------------------------------------------------
function docker_run::parse_cmd_line()
{
   while [[ $# -gt 0 ]]; do

      case "$1" in

         -h|--help)
            docker_run::usage
            exit
            ;;

         --image)
            IMAGE="$2"
            shift 2
            ;;

         --image=*)
            IMAGE="${1#*=}"
            shift
            ;;

         --include-gdb)
            docker_run::add_gdb_options
            shift
            ;;

         --include-icepic)
            docker_run::add_icepic_options
            shift
            ;;

         --include-workspace)
            docker_run::add_workspace_options
            shift
            ;;

         --login)
            docker_run::use_bash_login
            shift
            ;;

         -n|--dry-run)
            _EXE=docker_run::dry_run
            shift
            ;;

         -s|--ssh-auth-sock)
            docker_run::add_ssh_auth_sock_options
            shift
            ;;

         --)
            shift
            docker_run::append_cmd "$@"
            break
            ;;

         *)
            docker_run::append_option "$1"
            shift
            ;;

      esac

   done

   # Add container name if it wasn't already provided on the command line and
   # $CONTAINER_NAME has a value
   if docker_run::has_option "-it" \
      && ! docker_run::has_option --name \
      && [ -n "${CONTAINER_NAME}" ]; then

      docker_run::prepend_option --name "${CONTAINER_NAME}"
   fi

   # Use the default command if the '-it' option is present and $_CMD is empty
   if docker_run::has_option "-it" && (( ${#_CMD[@]} == 0 )); then
      docker_run::append_cmd "${DEFAULT_COMMAND[@]}"
   fi
}

#-------------------------------------------------------------------------------
# Same as the execute() function but print the 'docker run' command rather than
# execute it.
#
# Arguments:
# None
#-------------------------------------------------------------------------------
function docker_run::dry_run()
{
   echo "docker run "${_OPTIONS[@]}" "${IMAGE}" "${_CMD[@]}""
   echo
   echo "Options:"
   for arg in "${_OPTIONS[@]}"; do
      echo "${arg}"
   done
   echo
   echo "Image:"
   echo "${IMAGE}"
   echo
   echo "Command:"
   for arg in "${_CMD[@]}"; do
      echo "${arg}"
   done

   # The image name was not provided
   if [ -z "${IMAGE}" ]; then
      utilities::print_error "An image was not specified. Use the --image string option to specify an image."
   fi
}

#-------------------------------------------------------------------------------
# Execute the 'docker run' command.
#
# Arguments:
# None
#-------------------------------------------------------------------------------
function docker_run::execute()
{
   # The image name was not provided
   if [ -z "${IMAGE}" ]; then
      utilities::error "An image was not specified. Use the --image string option to specify an image."
   fi

   docker run "${_OPTIONS[@]}" "${IMAGE}" "${_CMD[@]}"
}

#-------------------------------------------------------------------------------
# Parse the command line arguments and execute the 'docker run' command.
#
# Arguments:
# $@ - command line arguments
#-------------------------------------------------------------------------------
function docker_run::run()
{
   if (( $# > 0 )); then
      docker_run::parse_cmd_line "$@"
   fi

   ${_EXE}
}