#!/bin/bash
#-------------------------------------------------------------------------------
# Filename: utilities.sh
#
# Description:
# General utility functions.
#-------------------------------------------------------------------------------
LIBRARY_PATH=$(cd "${BASH_SOURCE[0]%/*}" && pwd)

source "${LIBRARY_PATH}/utilities/array.sh"
source "${LIBRARY_PATH}/utilities/color.sh"
source "${LIBRARY_PATH}/utilities/container.sh"
source "${LIBRARY_PATH}/utilities/error.sh"
source "${LIBRARY_PATH}/utilities/import.sh"
source "${LIBRARY_PATH}/utilities/install.sh"
source "${LIBRARY_PATH}/utilities/path.sh"
source "${LIBRARY_PATH}/utilities/print.sh"
source "${LIBRARY_PATH}/utilities/shim.sh"
source "${LIBRARY_PATH}/utilities/ssh_agent.sh"
