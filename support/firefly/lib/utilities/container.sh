#!/bin/bash
#-------------------------------------------------------------------------------
# Filename: container.sh
#
# Description:
# Utility functions regarding containers.
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Determines if this process is running in a container.
#
# Arguments:
# None
#
# Returns:
# 0 - this process is in a container
# 1 - this process is not in a container
#-------------------------------------------------------------------------------
function utilities::in_a_container()
{
    [[ -f /.dockerenv ]]
}
