#!/bin/bash
#-------------------------------------------------------------------------------
# Filename: ssh_agent.sh
#
# Description:
# Utility functions to manage an ssh-agent.
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Starts an ssh-agent if one is not already started.
#
# Notes:
# * This function defines SSH_AUTH_SOCK
# * The socket will be placed in '~/.ssh/ssh-agent.sock'
# * This function can be called from .bash_profile
#
# Arguments:
# None
#-------------------------------------------------------------------------------
function utilities::start_ssh_agent()
{
    # Define SSH_AUTH_SOCK
    export SSH_AUTH_SOCK=~/.ssh/ssh-agent.sock

    # Attempt to connect to the ssh-agent
    ssh-add -l 2>/dev/null >/dev/null

    # Unable to connect to the ssh-agent
    if (( $? >= 2 )); then
        echo -e "${BOLD_BLUE}Starting ssh-agent...${NO_COLOR}"
        ssh-agent -a "${SSH_AUTH_SOCK}" >/dev/null

        echo -e "${BOLD_BLUE}Adding ssh keys...${NO_COLOR}"
        ssh-add
    else
        echo -e "${BOLD_BLUE}ssh-agent already started.${NO_COLOR}"
    fi
}

#-------------------------------------------------------------------------------
function utilities::stop_ssh_agent()
{
    pkill -u ${USER} ssh-agent
    unset SSH_AUTH_SOCK
}
