#!/bin/bash
#-------------------------------------------------------------------------------
# Filename: path.sh
#
# Description:
# Functions that will manipulate a variable that holds paths in the form of
# 'path1:path2:path3'.
#
# Source:
# https://heptapod.host/flowblok/shell-startup/-/blob/branch/default/.shell/env_functions
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Usage: indirect_expand PATH -> $PATH
#
# TODO: This function only works with exported variables that are visible with
#       the 'env' command.
#-------------------------------------------------------------------------------
function utilities::indirect_expand()
{
   env | sed -n "s/^$1=//p"
}

#-------------------------------------------------------------------------------
# Usage: pathremove /path/to/bin [PATH]
# Eg, to remove ~/bin from $PATH
#     pathremove ~/bin PATH
#
# TODO: This function only works with exported variables that are visible with
#       the 'env' command used in the utilities::indirect_expand function.
#-------------------------------------------------------------------------------
function utilities::remove_path()
{
   local IFS=':'
   local new_path
   local dir
   local var=${2:-PATH}
   # Bash has ${!var}, but this is not portable.
   for dir in $(utilities::indirect_expand "${var}"); do
      IFS=''
      if [[ "$dir" != "$1" ]]; then
         new_path="${new_path}":"${dir}"
      fi
   done
   export ${var}="${new_path#:}"
}

#-------------------------------------------------------------------------------
# Usage: pathprepend /path/to/bin [PATH]
# Eg, to prepend ~/bin to $PATH
#     pathprepend ~/bin PATH
#-------------------------------------------------------------------------------
function utilities::prepend_path()
{
   # if the path is already in the variable,
   # remove it so we can move it to the front
   utilities::remove_path "$1" "$2"
   [[ -d "${1}" ]] || return 0
   local var="${2:-PATH}"
   local value="$(utilities::indirect_expand "${var}")"
   export ${var}="${1}${value:+:${value}}"
}

#-------------------------------------------------------------------------------
# Usage: pathappend /path/to/bin [PATH]
# Eg, to append ~/bin to $PATH
#     pathappend ~/bin PATH
#-------------------------------------------------------------------------------
function utilities::append_path()
{
   utilities::remove_path "$1" "$2"
   [[ -d "$1" ]] || return 0
   local var=${2:-PATH}
   local value="$(utilities::indirect_expand "${var}")"
   export ${var}="${value:+${value}:}$1"
}
