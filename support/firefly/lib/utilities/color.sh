#!/bin/bash
#-------------------------------------------------------------------------------
# Filename: color.sh
#
# Description:
# Define various ANSI colors.
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Example ansi escape sequence:
# \e[0;30m
#
# \e[       begin color modification
# 0;        ANSI code for normal, bold, etc
# 30m       color code followed by 'm'
#
# \e[0m     reset color
#
# STYLE         ANSI Code
#-------------- ---------
# normal            0
# bold              1
# underline         4
# blinking          5
# reverse video     7
#
# Add 60 to any color code to get HIGH INTENSITY
#
# COLOR          FG      BG
#-------------  ----    ----
# black          30      40
# red            31      41
# green          32      42
# yellow         33      43
# blue           34      44
# purple         35      45
# cyan           36      46
# white          37      47
#
# dark gray     1;30    1;40
# light red     1;31    1;41
# light green   1;32    1;42
# yellow        1;33    1;43
# light blue    1;34    1;44
# light purple  1;35    1;45
# light cyan    1;36    1;46
# white         1;37    1;47
#
# Use `echo -e` to apply color codes using the echo command
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Color definitions
declare -r BLACK='\e[0;30m'
declare -r RED='\e[0;31m'
declare -r GREEN='\e[0;32m'
declare -r YELLOW='\e[0;33m'
declare -r BLUE='\e[0;34m'
declare -r PURPLE='\e[0;35m'
declare -r CYAN='\e[0;36m'
declare -r WHITE='\e[0;37m'

declare -r BOLD_GRAY='\e[1;30m'
declare -r BOLD_RED='\e[1;31m'
declare -r BOLD_GREEN='\e[1;32m'
declare -r BOLD_YELLOW='\e[1;33m'
declare -r BOLD_BLUE='\e[1;34m'
declare -r BOLD_PURPLE='\e[1;35m'
declare -r BOLD_CYAN='\e[1;36m'
declare -r BOLD_WHITE='\e[1;37m'

declare -r NO_COLOR='\e[0m'

#-------------------------------------------------------------------------------
# Set console color.
#
# Arguments:
# $1 - name of color
#
# Usage:
# utilities::set_color BOLD_RED
#-------------------------------------------------------------------------------
function utilities::set_color()
{
    local -r color="$1"
    echo -en "${!color}"
}

#-------------------------------------------------------------------------------
# Reset the console color.
#
# Arguments:
# None
#-------------------------------------------------------------------------------
function utilities::reset_color()
{
    utilities::set_color NO_COLOR
}