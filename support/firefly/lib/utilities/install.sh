#!/bin/bash
#-------------------------------------------------------------------------------
# Filename: install.sh
#
# Description:
# Functions to help with installing files.
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Link a file to the home directory.
#
# Arguments:
# $1 - file
#-------------------------------------------------------------------------------
function utilities::link_file()
{
   local -r src="$1"
   local -r dst=~/"$1"

   if [[ -h "${dst}" ]]; then
      echo "Skipping ${dst}. File already linked."
      return
   fi

   if [[ -e "${dst}" ]]; then
      echo "Copying ${dst} to repo..."
      cp ${dst} ${src}
   fi

   echo "Linking ${dst}..."
   ln -sf "$(realpath "${src}")" "${dst}"
}

#-------------------------------------------------------------------------------
# Unlink a file in the home directory.
#
# Arguments:
# $1 - file
#-------------------------------------------------------------------------------
function utilities::unlink_file()
{
   local -r src="$1"
   local -r dst=~/"$1"

   if [[ ! -h "${dst}" ]]; then
      echo "Skipping ${dst}. File is not linked."
      return
   fi

   echo "Unlinking ${dst}..."
   rm "${dst}"

   echo "Copying ${dst} from repo..."
   cp "${src}" "${dst}"
}

#-------------------------------------------------------------------------------
# Copy a file to the home directory.
#
# Arguments:
# $1 - file
#-------------------------------------------------------------------------------
function utilities::copy_file()
{
   local -r src="$1"
   local -r dst=~/"$1"

   if [[ ! -e "${dst}" ]]; then
      echo "Copying ${dst} from repo..."
      cp "${src}" "${dst}"
   else
      echo "Skipping ${dst}. File already exists."
   fi
}
