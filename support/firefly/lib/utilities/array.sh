#!/bin/bash
#-------------------------------------------------------------------------------
# Filename: array.sh
#
# Description:
# Functions that operate on bash arrays.
#-------------------------------------------------------------------------------
UTILITIES_PATH=$(cd "${BASH_SOURCE[0]%/*}" && pwd)
source "${UTILITIES_PATH}/array.sh.${BASH_VERSINFO}"
