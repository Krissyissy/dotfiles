#!/bin/bash
#-------------------------------------------------------------------------------
# Filename: import.sh
#
# Description:
# Functions to source bash files.
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Source a bash file script.
#
# Arguments:
# $1 - file to import
# $@ - arguments to pass to the file being imported
#-------------------------------------------------------------------------------
function utilities::import()
{
   local -r file="$1"
   shift
   source "${file}" "$@"
}

#-------------------------------------------------------------------------------
# Source a given file if it exists.
#
# Arguments:
# $1 - file to be sourced
#
# Usage:
# try_source filename
#-------------------------------------------------------------------------------
function utilities::try_source()
{
   if [[ -r "$1" ]]; then
      source "$1"
   fi
}
