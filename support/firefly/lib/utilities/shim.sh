#!/bin/bash
#-------------------------------------------------------------------------------
# Filename: shim.sh
#
# Description:
# Bash shims.
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Determines if a function has been defined.
#
# Arguments:
# $1 - function name
#
# Usage:
# fn_defined f && echo yes || echo no
#-------------------------------------------------------------------------------
function utilities::is_function_defined()
{
    declare -F "$1" > /dev/null;
}
