# ~/.bashrc: executed by bash(1) for non-login shells.

# If not running interactively, don't do anything
#[[ "$-" != *i* ]] && return

declare -r DOTFILES_DIR=~/.dotfiles
declare -r FIREFLY_DIR="${DOTFILES_DIR}"/support/firefly

. ${FIREFLY_DIR}/lib/utilities.sh

# Inherit from /etc/bashrc if it exists
utilities::try_source /etc/bashrc

# aliases
utilities::try_source ${DOTFILES_DIR}/.bash_aliases

# bash_completion
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# editor
# EDITOR="vim"

# history
HISTCONTROL=ignoreboth
HISTSIZE=16384
shopt -s histappend

# prompt
utilities::try_source /usr/share/git-core/contrib/completion/git-prompt.sh

if utilities::is_function_defined __git_ps1; then
    PS1='\[\033[32m\]\u@\h \[\033[33m\]\w\[\033[31m\]$(__git_ps1 " (%s)")\[\033[0;94m\]\$\[\033[0m\] '
else
    PS1='\[\033[32m\]\u@\h \[\033[33m\]\w\[\033[0;94m\]\$\[\033[0m\] '
fi

# shell options

# Check the window size after each command and, if necessary, update the values
# of LINES and COLUMNS.
shopt -s checkwinsize
